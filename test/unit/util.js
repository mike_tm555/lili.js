(function() {

  QUnit.module('lil.util');

  function _createImageElement() {
    return lil.document.createElement('img');
  }

  function getAbsolutePath(path) {
    var isAbsolute = /^https?:/.test(path);
    if (isAbsolute) { return path; };
    var imgEl = _createImageElement();
    imgEl.src = path;
    var src = imgEl.src;
    imgEl = null;
    return src;
  }

  var IMG_URL = lil.isLikelyNode
    ? 'file://' + require('path').join(__dirname, '../fixtures/', 'very_large_image.jpg')
    : getAbsolutePath('../fixtures/very_large_image.jpg');

  var IMG_URL_NON_EXISTING = 'http://www.google.com/non-existing';

  QUnit.test('lil.util.toFixed', function(assert) {
    assert.ok(typeof lil.util.toFixed === 'function');

    function test(what) {
      assert.equal(lil.util.toFixed(what, 2), 166.67, 'should leave 2 fractional digits');
      assert.equal(lil.util.toFixed(what, 5), 166.66667, 'should leave 5 fractional digits');
      assert.equal(lil.util.toFixed(what, 0), 167, 'should leave 0 fractional digits');

      var fractionless = (typeof what == 'number')
        ? parseInt(what)
        : what.substring(0, what.indexOf('.'));

      assert.equal(lil.util.toFixed(fractionless, 2), 166, 'should leave fractionless number as is');
    }

    test.call(this, '166.66666666666666666666'); // string
    test.call(this, 166.66666666666666666666); // number
  });

  QUnit.test('lil.util.removeFromArray', function(assert) {
    var testArray = [1,2,3,4,5];

    assert.ok(typeof lil.util.removeFromArray === 'function');

    lil.util.removeFromArray(testArray, 2);
    assert.deepEqual([1,3,4,5], testArray);
    assert.equal(lil.util.removeFromArray(testArray, 1), testArray, 'should return reference to original array');

    testArray = [1,2,3,1];
    lil.util.removeFromArray(testArray, 1);
    assert.deepEqual([2,3,1], testArray, 'only first occurance of value should be deleted');

    testArray = [1,2,3];
    lil.util.removeFromArray(testArray, 12);
    assert.deepEqual([1,2,3], testArray, 'deleting unexistent value should not affect array');

    testArray = [];
    lil.util.removeFromArray(testArray, 1);
    assert.deepEqual([], testArray, 'deleting any value from empty array should not affect it');

    testArray = ['0'];
    lil.util.removeFromArray(testArray, 0);
    assert.deepEqual(['0'], testArray, 'should use (strict) identity comparison, rather than equality one');
  });

  QUnit.test('lil.util.degreesToRadians', function(assert) {
    assert.ok(typeof lil.util.degreesToRadians === 'function');
    assert.equal(lil.util.degreesToRadians(0), 0);
    assert.equal(lil.util.degreesToRadians(90), Math.PI / 2);
    assert.equal(lil.util.degreesToRadians(180), Math.PI);

    assert.deepEqual(lil.util.degreesToRadians(), NaN);
  });

  QUnit.test('lil.util.radiansToDegrees', function(assert) {
    assert.ok(typeof lil.util.radiansToDegrees === 'function');

    assert.equal(lil.util.radiansToDegrees(0), 0);
    assert.equal(lil.util.radiansToDegrees(Math.PI / 2), 90);
    assert.equal(lil.util.radiansToDegrees(Math.PI), 180);

    assert.deepEqual(lil.util.radiansToDegrees(), NaN);
  });

  QUnit.test('lil.util.getRandomInt', function(assert) {
    assert.ok(typeof lil.util.getRandomInt === 'function');

    var randomInts = [];
    for (var i = 100; i--; ) {
      var randomInt = lil.util.getRandomInt(100, 200);
      randomInts.push(randomInt);
      assert.ok(randomInt >= 100 && randomInt <= 200);
    }

    var areAllTheSame = randomInts.every(function(value){
      return value === randomInts[0];
    });

    assert.ok(!areAllTheSame);
  });

  QUnit.test('lil.util.falseFunction', function(assert) {
    assert.ok(typeof lil.util.falseFunction === 'function');
    assert.equal(lil.util.falseFunction(), false);
  });

  QUnit.test('String.prototype.trim', function(assert) {
    assert.ok(typeof String.prototype.trim === 'function');
    assert.equal('\t\n   foo bar \n    \xA0  '.trim(), 'foo bar');
  });

  QUnit.test('lil.util.string.camelize', function(assert) {
    var camelize = lil.util.string.camelize;

    assert.ok(typeof camelize === 'function');

    assert.equal(camelize('foo'), 'foo');
    assert.equal(camelize('foo-bar'), 'fooBar');
    assert.equal(camelize('Foo-bar-Baz'), 'FooBarBaz');
    assert.equal(camelize('FooBarBaz'), 'FooBarBaz');
    assert.equal(camelize('-bar'), 'Bar');
    assert.equal(camelize(''), '');
    assert.equal(camelize('and_something_with_underscores'), 'and_something_with_underscores');
    assert.equal(camelize('underscores_and-dashes'), 'underscores_andDashes');
    assert.equal(camelize('--double'), 'Double');
  });

  QUnit.test('lil.util.string.graphemeSplit', function(assert) {
    var gSplit = lil.util.string.graphemeSplit;

    assert.ok(typeof gSplit === 'function');

    assert.deepEqual(gSplit('foo'), ['f', 'o', 'o'], 'normal test get splitted by char');
    assert.deepEqual(gSplit('f🙂o'), ['f', '🙂', 'o'], 'normal test get splitted by char');
  });

  QUnit.test('lil.util.string.escapeXml', function(assert) {
    var escapeXml = lil.util.string.escapeXml;

    assert.ok(typeof escapeXml === 'function');

    // borrowed from Prototype.js
    assert.equal('foo bar', escapeXml('foo bar'));
    assert.equal('foo &lt;span&gt;bar&lt;/span&gt;', escapeXml('foo <span>bar</span>'));
    //equal('foo ß bar', escapeXml('foo ß bar'));

    //equal('ウィメンズ2007\nクルーズコレクション', escapeXml('ウィメンズ2007\nクルーズコレクション'));

    assert.equal('a&lt;a href=&quot;blah&quot;&gt;blub&lt;/a&gt;b&lt;span&gt;&lt;div&gt;&lt;/div&gt;&lt;/span&gt;cdef&lt;strong&gt;!!!!&lt;/strong&gt;g',
      escapeXml('a<a href="blah">blub</a>b<span><div></div></span>cdef<strong>!!!!</strong>g'));

    assert.equal('1\n2', escapeXml('1\n2'));
  });

  QUnit.test('lil.util.string.capitalize', function(assert) {
    var capitalize = lil.util.string.capitalize;

    assert.ok(typeof capitalize === 'function');

    assert.equal(capitalize('foo'), 'Foo');
    assert.equal(capitalize(''), '');
    assert.equal(capitalize('Foo'), 'Foo');
    assert.equal(capitalize('foo-bar-baz'), 'Foo-bar-baz');
    assert.equal(capitalize('FOO'), 'Foo');
    assert.equal(capitalize('FoobaR'), 'Foobar');
    assert.equal(capitalize('2foo'), '2foo');
  });

  QUnit.test('lil.util.object.extend', function(assert) {
    var extend = lil.util.object.extend;

    assert.ok(typeof extend === 'function');

    var destination = { x: 1 },
        source = { y: 2 };

    extend(destination, source);

    assert.equal(destination.x, 1);
    assert.equal(destination.y, 2);
    assert.equal(source.x, undefined);
    assert.equal(source.y, 2);

    destination = { x: 1 };
    source = { x: 2 };

    extend(destination, source);

    assert.equal(destination.x, 2);
    assert.equal(source.x, 2);
  });

  QUnit.test('lil.util.object.extend deep', function(assert) {
    var extend = lil.util.object.extend;
    var d = function() { };
    var destination = { x: 1 },
        source = { y: 2, a: { b: 1, c: [1, 2, 3, d] } };

    extend(destination, source, true);

    assert.equal(destination.x, 1, 'x is still in destination');
    assert.equal(destination.y, 2, 'y has been added');
    assert.deepEqual(destination.a, source.a, 'a has been copied deeply');
    assert.notEqual(destination.a, source.a, 'is not the same object');
    assert.ok(typeof source.a.c[3] === 'function', 'is a function');
    assert.equal(destination.a.c[3], source.a.c[3], 'functions get referenced');
  });

  QUnit.test('lil.util.object.clone', function(assert) {
    var clone = lil.util.object.clone;

    assert.ok(typeof clone === 'function');

    var obj = { x: 1, y: [1, 2, 3] },
        _clone = clone(obj);

    assert.equal(_clone.x, 1);
    assert.notEqual(obj, _clone);
    assert.equal(_clone.y, obj.y);
  });

  QUnit.test('Function.prototype.bind', function(assert) {
    assert.ok(typeof Function.prototype.bind === 'function');

    var obj = { };
    function fn() {
      return [this, arguments[0], arguments[1]];
    }

    var bound = fn.bind(obj);
    assert.deepEqual([obj, undefined, undefined], bound());
    assert.deepEqual([obj, 1, undefined], bound(1));
    assert.deepEqual([obj, 1, null], bound(1, null));

    bound = fn.bind(obj, 1);
    assert.deepEqual([obj, 1, undefined], bound());
    assert.deepEqual([obj, 1, 2], bound(2));

    function Point(x, y) {
      this.x = x;
      this.y = y;
    }

    obj = { };
    var YAxisPoint = Point.bind(obj, 0);
    var axisPoint = new YAxisPoint(5);

    assert.deepEqual(0, axisPoint.x);
    assert.deepEqual(5, axisPoint.y);

    assert.ok(axisPoint instanceof Point);
    // assert.ok(axisPoint instanceof YAxisPoint); <-- fails
  });

  QUnit.test('lil.util.getById', function(assert) {
    assert.ok(typeof lil.util.getById === 'function');

    var el = lil.document.createElement('div');
    el.id = 'foobarbaz';
    lil.document.body.appendChild(el);

    assert.equal(el, lil.util.getById(el));
    assert.equal(el, lil.util.getById('foobarbaz'));
    assert.equal(null, lil.util.getById('likely-non-existent-id'));
  });

  QUnit.test('lil.util.toArray', function(assert) {
    assert.ok(typeof lil.util.toArray === 'function');

    assert.deepEqual(['x', 'y'], lil.util.toArray({ 0: 'x', 1: 'y', length: 2 }));
    assert.deepEqual([1, 3], lil.util.toArray((function(){ return arguments; })(1, 3)));

    var nodelist = lil.document.getElementsByTagName('div'),
        converted = lil.util.toArray(nodelist);

    assert.ok(converted instanceof Array);
    assert.equal(nodelist.length, converted.length);
    assert.equal(nodelist[0], converted[0]);
    assert.equal(nodelist[1], converted[1]);
  });

  QUnit.test('lil.util.makeElement', function(assert) {
    var makeElement = lil.util.makeElement;
    assert.ok(typeof makeElement === 'function');

    var el = makeElement('div');

    assert.equal(el.tagName.toLowerCase(), 'div');
    assert.equal(el.nodeType, 1);

    el = makeElement('p', { 'class': 'blah', 'for': 'boo_hoo', 'some_random-attribute': 'woot' });

    assert.equal(el.tagName.toLowerCase(), 'p');
    assert.equal(el.nodeType, 1);
    assert.equal(el.className, 'blah');
    assert.equal(el.htmlFor, 'boo_hoo');
    assert.equal(el.getAttribute('some_random-attribute'), 'woot');
  });

  QUnit.test('lil.util.addClass', function(assert) {
    var addClass = lil.util.addClass;
    assert.ok(typeof addClass === 'function');

    var el = lil.document.createElement('div');
    addClass(el, 'foo');
    assert.equal(el.className, 'foo');

    addClass(el, 'bar');
    assert.equal(el.className, 'foo bar');

    addClass(el, 'baz qux');
    assert.equal(el.className, 'foo bar baz qux');

    addClass(el, 'foo');
    assert.equal(el.className, 'foo bar baz qux');
  });

  QUnit.test('lil.util.wrapElement', function(assert) {
    var wrapElement = lil.util.wrapElement;
    assert.ok(typeof wrapElement === 'function');

    var el = lil.document.createElement('p');
    var wrapper = wrapElement(el, 'div');

    assert.equal(wrapper.tagName.toLowerCase(), 'div');
    assert.equal(wrapper.firstChild, el);

    el = lil.document.createElement('p');
    wrapper = wrapElement(el, 'div', { 'class': 'foo' });

    assert.equal(wrapper.tagName.toLowerCase(), 'div');
    assert.equal(wrapper.firstChild, el);
    assert.equal(wrapper.className, 'foo');

    var childEl = lil.document.createElement('span');
    var parentEl = lil.document.createElement('p');

    parentEl.appendChild(childEl);

    wrapper = wrapElement(childEl, 'strong');

    // wrapper is now in between parent and child
    assert.equal(wrapper.parentNode, parentEl);
    assert.equal(wrapper.firstChild, childEl);
  });

  QUnit.test('lil.util.makeElementUnselectable', function(assert) {
    var makeElementUnselectable = lil.util.makeElementUnselectable;

    assert.ok(typeof makeElementUnselectable === 'function');

    var el = lil.document.createElement('p');
    el.appendChild(lil.document.createTextNode('foo'));

    assert.equal(el, makeElementUnselectable(el), 'should be "chainable"');
    if (typeof el.onselectstart !== 'undefined') {
      assert.equal(el.onselectstart, lil.util.falseFunction);
    }

    // not sure if it's a good idea to test implementation details here
    // functional test would probably make more sense
    if (typeof el.unselectable === 'string') {
      assert.equal('on', el.unselectable);
    }
    else if (typeof el.userSelect !== 'undefined') {
      assert.equal('none', el.userSelect);
    }
  });

  QUnit.test('lil.util.makeElementSelectable', function(assert) {
    var makeElementSelectable = lil.util.makeElementSelectable,
        makeElementUnselectable = lil.util.makeElementUnselectable;

    assert.ok(typeof makeElementSelectable === 'function');

    var el = lil.document.createElement('p');
    el.appendChild(lil.document.createTextNode('foo'));

    makeElementUnselectable(el);
    makeElementSelectable(el);

    if (typeof el.onselectstart !== 'undefined') {
      assert.equal(el.onselectstart, null);
    }
    if (typeof el.unselectable === 'string') {
      assert.equal('', el.unselectable);
    }
    else if (typeof el.userSelect !== 'undefined') {
      assert.equal('', el.userSelect);
    }
  });

  QUnit.test('lil.loadSVGFromURL', function(assert) {
    assert.equal('function', typeof lil.loadSVGFromURL);
  });

  var SVG_DOC_AS_STRING = '<?xml version="1.0"?>\
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">\
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
      <polygon fill="red" stroke="blue" stroke-width="10" points="350, 75 379,161 469,161\
        397,215 423,301 350,250 277,301 303,215 231,161 321,161" />\
    </svg>';

  QUnit.test('lil.loadSVGFromString', function(assert) {
    var done = assert.async();
    assert.equal('function', typeof lil.loadSVGFromString);

    lil.loadSVGFromString(SVG_DOC_AS_STRING, function(loadedObjects) {
      assert.ok(loadedObjects[0] instanceof lil.Polygon);
      assert.equal('red', loadedObjects[0].fill);
      setTimeout(done, 1000);
    });
  });

  QUnit.test('lil.loadSVGFromString with surrounding whitespace', function(assert) {
    var done = assert.async();
    var loadedObjects = [];
    lil.loadSVGFromString('   \n\n  ' + SVG_DOC_AS_STRING + '  ', function(objects) {
      loadedObjects = objects;
    });

    setTimeout(function() {
      assert.ok(loadedObjects[0] instanceof lil.Polygon);
      assert.equal('red', loadedObjects[0] && loadedObjects[0].fill);
      done();
    }, 1000);
  });

  QUnit.test('lil.util.loadImage', function(assert) {
    var done = assert.async();
    assert.ok(typeof lil.util.loadImage === 'function');

    if (IMG_URL.indexOf('/home/travis') === 0) {
      // image can not be accessed on travis so we're returning early
      done();
      return;
    }

    lil.util.loadImage(IMG_URL, function(obj) {
      if (obj) {
        var oImg = new lil.Image(obj);
        assert.ok(/fixtures\/very_large_image\.jpg$/.test(oImg.getSrc()), 'image should have correct src');
      }
      done();
    });
  });

  QUnit.test('lil.util.loadImage with no args', function(assert) {
    var done = assert.async();
    if (IMG_URL.indexOf('/home/travis') === 0) {
      // image can not be accessed on travis so we're returning early
      assert.expect(0);
      done();
      return;
    }

    lil.util.loadImage('', function() {
      assert.ok(1, 'callback should be invoked');
      done();
    });
  });

  QUnit.test('lil.util.loadImage with crossOrigin', function(assert) {
    var done = assert.async();
    if (IMG_URL.indexOf('/home/travis') === 0) {
      // image can not be accessed on travis so we're returning early
      assert.expect(0);
      done();
      return;
    }
    try {
      lil.util.loadImage(IMG_URL, function(img) {
        assert.equal(img.src, IMG_URL, 'src is set');
        assert.equal(img.crossOrigin, 'anonymous', 'crossOrigin is set');
        done();
      }, null, 'anonymous');
    }
    catch (e) {
      console.log(e);
    }
  });


  QUnit.test('lil.util.loadImage with url for a non exsiting image', function(assert) {
    var done = assert.async();
    try {
      lil.util.loadImage(IMG_URL_NON_EXISTING, function(img, error) {
        assert.equal(error, true, 'callback should be invoked with error set to true');
        done();
      }, this);
    }
    catch (e) { }
  });

  var SVG_WITH_1_ELEMENT = '<?xml version="1.0"?>\
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">\
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
      <polygon fill="red" stroke="blue" stroke-width="10" points="350, 75 379,161 469,161\
        397,215 423,301 350,250 277,301 303,215 231,161 321,161" />\
    </svg>';

  var SVG_WITH_2_ELEMENTS = '<?xml version="1.0"?>\
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">\
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
      <polygon fill="red" stroke="blue" stroke-width="10" points="350, 75 379,161 469,161\
        397,215 423,301 350,250 277,301 303,215 231,161 321,161" />\
      <polygon fill="red" stroke="blue" stroke-width="10" points="350, 75 379,161 469,161\
        397,215 423,301 350,250 277,301 303,215 231,161 321,161" />\
    </svg>';

  QUnit.test('lil.util.groupSVGElements', function(assert) {
    var done = assert.async();
    assert.ok(typeof lil.util.groupSVGElements === 'function');

    var group1;
    lil.loadSVGFromString(SVG_WITH_1_ELEMENT, function(objects, options) {
      group1 = lil.util.groupSVGElements(objects, options);
      assert.ok(group1 instanceof lil.Polygon, 'it returns just the first element in case is just one');
      done();
    });
  });

  QUnit.test('lil.util.groupSVGElements #2', function(assert) {
    var done = assert.async();
    var group2;
    lil.loadSVGFromString(SVG_WITH_2_ELEMENTS, function(objects, options) {
      group2 = lil.util.groupSVGElements(objects, options);
      assert.ok(group2 instanceof lil.Group);
      done();
    });
  });

  QUnit.test('lil.util.createClass', function(assert) {
    var Klass = lil.util.createClass();

    assert.ok(typeof Klass === 'function');
    assert.ok(typeof new Klass() === 'object');

    var Person = lil.util.createClass({
      initialize: function(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
      },
      toString: function() {
        return 'My name is ' + this.firstName + ' ' + this.lastName;
      }
    });

    assert.ok(typeof Person === 'function');
    assert.ok(typeof new Person() === 'object');

    var john = new Person('John', 'Meadows');
    assert.ok(john instanceof Person);

    assert.equal(john.firstName, 'John');
    assert.equal(john.lastName, 'Meadows');
    assert.equal(john + '', 'My name is John Meadows');


    var WebDeveloper = lil.util.createClass(Person, {
      initialize: function(firstName, lastName, skills) {
        this.callSuper('initialize', firstName, lastName);
        this.skills = skills;
      },
      toString: function() {
        return this.callSuper('toString') + ' and my skills are ' + this.skills.join(', ');
      }
    });

    assert.ok(typeof WebDeveloper === 'function');
    var dan = new WebDeveloper('Dan', 'Trink', ['HTML', 'CSS', 'Javascript']);
    assert.ok(dan instanceof Person);
    assert.ok(dan instanceof WebDeveloper);

    assert.equal(dan.firstName, 'Dan');
    assert.equal(dan.lastName, 'Trink');
    assert.deepEqual(dan.skills, ['HTML', 'CSS', 'Javascript']);

    assert.equal(dan + '', 'My name is Dan Trink and my skills are HTML, CSS, Javascript');
  });

  // element doesn't seem to have style on node
  if (!lil.isLikelyNode) {
    QUnit.test('lil.util.setStyle', function(assert) {

      assert.ok(typeof lil.util.setStyle === 'function');

      var el = lil.document.createElement('div');

      lil.util.setStyle(el, 'color:red');
      assert.equal(el.style.color, 'red');
    });
  }

  QUnit.test('lil.util.addListener', function(assert) {
    assert.ok(typeof lil.util.addListener === 'function', 'lil.util.addListener is a function');
    lil.util.addListener(null, 'mouseup');
    assert.ok(true, 'test did not throw on null element addListener');
  });

  QUnit.test('lil.util.removeListener', function(assert) {
    assert.ok(typeof lil.util.removeListener === 'function', 'lil.util.removeListener is a function');
    lil.util.removeListener(null, 'mouseup');
    assert.ok(true, 'test did not throw on null element removeListener');
  });

  QUnit.test('lil.util.drawDashedLine', function(assert) {
    assert.ok(typeof lil.util.drawDashedLine === 'function');

    var canvas = new lil.StaticCanvas(null, {enableRetinaScaling: false});


    var ctx = canvas.getContext('2d');

    lil.util.drawDashedLine(ctx, 0, 0, 100, 100, [5, 5]);
  });

  QUnit.test('lil.util.array.invoke', function(assert) {
    assert.ok(typeof lil.util.array.invoke === 'function');

    var obj1 = { toString: function(){ return 'obj1'; } };
    var obj2 = { toString: function(){ return 'obj2'; } };
    var obj3 = { toString: function(){ return 'obj3'; } };

    assert.deepEqual(['obj1', 'obj2', 'obj3'],
      lil.util.array.invoke([obj1, obj2, obj3], 'toString'));

    assert.deepEqual(['f', 'b', 'b'],
      lil.util.array.invoke(['foo', 'bar', 'baz'], 'charAt', 0));

    assert.deepEqual(['o', 'a', 'a'],
      lil.util.array.invoke(['foo', 'bar', 'baz'], 'charAt', 1));
  });

  QUnit.test('lil.util.array.min', function(assert) {
    assert.ok(typeof lil.util.array.min === 'function');

    assert.equal(1, lil.util.array.min([1, 3, 2]));
    assert.equal(-1, lil.util.array.min([3, 1, 'f', 3, -1, 3]));
    assert.equal(-3, lil.util.array.min([-1, -2, -3]));
    assert.equal('a', lil.util.array.min(['a', 'c', 'b']));

    var obj1 = { valueOf: function(){ return 1; } };
    var obj2 = { valueOf: function(){ return 2; } };
    var obj3 = { valueOf: function(){ return 3; } };

    assert.equal(obj1, lil.util.array.min([obj1, obj3, obj2]));
  });

  QUnit.test('lil.util.array.max', function(assert) {
    assert.ok(typeof lil.util.array.max === 'function');

    assert.equal(3, lil.util.array.max([1, 3, 2]));
    assert.equal(3, lil.util.array.max([3, 1, 'f', 3, -1, 3]));
    assert.equal(-1, lil.util.array.max([-1, -2, -3]));
    assert.equal('c', lil.util.array.max(['a', 'c', 'b']));

    var obj1 = { valueOf: function(){ return 1; } };
    var obj2 = { valueOf: function(){ return 2; } };
    var obj3 = { valueOf: function(){ return 3; } };

    assert.equal(obj3, lil.util.array.max([obj1, obj3, obj2]));
  });

  QUnit.test('lil.util.populateWithProperties', function(assert) {
    assert.ok(typeof lil.util.populateWithProperties === 'function');

    var source = {
          foo: 'bar',
          baz: 123,
          qux: function() { }
        },
        destination = { };

    lil.util.populateWithProperties(source, destination);
    assert.ok(typeof destination.foo === 'undefined');
    assert.ok(typeof destination.baz === 'undefined');
    assert.ok(typeof destination.qux === 'undefined');

    lil.util.populateWithProperties(source, destination, ['foo']);
    assert.equal(destination.foo, 'bar');
    assert.ok(typeof destination.baz === 'undefined');
    assert.ok(typeof destination.qux === 'undefined');

    lil.util.populateWithProperties(source, destination, ['foo', 'baz', 'ffffffffff']);
    assert.equal(destination.foo, 'bar');
    assert.equal(destination.baz, 123);
    assert.ok(typeof destination.qux === 'undefined');
    assert.ok(typeof destination.ffffffffff === 'undefined');
  });

  QUnit.test('lil.util.getFunctionBody', function(assert) {
    assert.equal(lil.util.getFunctionBody('function(){}'), '');

    assert.equal(lil.util.getFunctionBody('function(){return 1 + 2}'),
      'return 1 + 2');

    assert.equal(lil.util.getFunctionBody('function () {\n  return "blah" }'),
      '\n  return "blah" ');

    assert.equal(lil.util.getFunctionBody('function foo (a , boo_bar, baz123 )\n{\n if (1) { alert(12345) } }'),
      '\n if (1) { alert(12345) } ');
  });

  QUnit.test('getKlass', function(assert) {
    assert.equal(lil.util.getKlass('circle'), lil.Circle);
    assert.equal(lil.util.getKlass('rect'), lil.Rect);
    assert.equal(lil.util.getKlass('RemoveWhite', 'lil.Image.filters'), lil.Image.filters.RemoveWhite);
    assert.equal(lil.util.getKlass('Sepia2', 'lil.Image.filters'), lil.Image.filters.Sepia2);
  });

  QUnit.test('resolveNamespace', function(assert) {
    assert.equal(lil.util.resolveNamespace('lil'), lil);
    assert.equal(lil.util.resolveNamespace('lil.Image'), lil.Image);
    assert.equal(lil.util.resolveNamespace('lil.Image.filters'), lil.Image.filters);
  });

  QUnit.test('clearlilFontCache', function(assert) {
    assert.ok(typeof lil.util.clearlilFontCache === 'function');
    lil.charWidthsCache = { arial: { some: 'cache'}, helvetica: { some: 'cache'} };
    lil.util.clearlilFontCache('arial');
    assert.equal(lil.charWidthsCache.arial,  undefined, 'arial cache is deleted');
    assert.equal(lil.charWidthsCache.helvetica.some, 'cache', 'helvetica cache is still available');
    lil.util.clearlilFontCache();
    assert.deepEqual(lil.charWidthsCache, { }, 'all cache is deleted');
  });

  QUnit.test('clearlilFontCache wrong case', function(assert) {
    lil.charWidthsCache = { arial: { some: 'cache'}, helvetica: { some: 'cache'} };
    lil.util.clearlilFontCache('ARIAL');
    assert.equal(lil.charWidthsCache.arial,  undefined, 'arial cache is deleted');
    assert.equal(lil.charWidthsCache.helvetica.some, 'cache', 'helvetica cache is still available');
  });

  QUnit.test('parsePreserveAspectRatioAttribute', function(assert) {
    assert.ok(typeof lil.util.parsePreserveAspectRatioAttribute === 'function');
    var parsed;
    parsed = lil.util.parsePreserveAspectRatioAttribute('none');
    assert.equal(parsed.meetOrSlice, 'meet');
    assert.equal(parsed.alignX, 'none');
    assert.equal(parsed.alignY, 'none');
    parsed = lil.util.parsePreserveAspectRatioAttribute('none slice');
    assert.equal(parsed.meetOrSlice, 'slice');
    assert.equal(parsed.alignX, 'none');
    assert.equal(parsed.alignY, 'none');
    parsed = lil.util.parsePreserveAspectRatioAttribute('XmidYmax meet');
    assert.equal(parsed.meetOrSlice, 'meet');
    assert.equal(parsed.alignX, 'mid');
    assert.equal(parsed.alignY, 'max');
  });

  QUnit.test('multiplyTransformMatrices', function(assert) {
    assert.ok(typeof lil.util.multiplyTransformMatrices === 'function');
    var m1 = [1, 1, 1, 1, 1, 1], m2 = [1, 1, 1, 1, 1, 1], m3;
    m3 = lil.util.multiplyTransformMatrices(m1, m2);
    assert.deepEqual(m3, [2, 2, 2, 2, 3, 3]);
    m3 = lil.util.multiplyTransformMatrices(m1, m2, true);
    assert.deepEqual(m3, [2, 2, 2, 2, 0, 0]);
  });

  QUnit.test('customTransformMatrix', function(assert) {
    assert.ok(typeof lil.util.customTransformMatrix === 'function');
    var m1 = lil.util.customTransformMatrix(5, 4, 45);
    assert.deepEqual(m1, [5, 0, 4.999999999999999, 4, 0, 0]);
  });

  QUnit.test('resetObjectTransform', function(assert) {
    assert.ok(typeof lil.util.resetObjectTransform === 'function');
    var rect = new lil.Rect({
      top: 1,
      width: 100,
      height: 100,
      angle: 30,
      scaleX: 2,
      scaleY: 1,
      flipX: true,
      flipY: true,
      skewX: 30,
      skewY: 30
    });
    assert.equal(rect.skewX, 30);
    assert.equal(rect.skewY, 30);
    assert.equal(rect.scaleX, 2);
    assert.equal(rect.scaleY, 1);
    assert.equal(rect.flipX, true);
    assert.equal(rect.flipY, true);
    assert.equal(rect.angle, 30);
    lil.util.resetObjectTransform(rect);
    assert.equal(rect.skewX, 0);
    assert.equal(rect.skewY, 0);
    assert.equal(rect.scaleX, 1);
    assert.equal(rect.scaleY, 1);
    assert.equal(rect.flipX, false);
    assert.equal(rect.flipY, false);
    assert.equal(rect.angle, 0);
  });

  QUnit.test('saveObjectTransform', function(assert) {
    assert.ok(typeof lil.util.saveObjectTransform === 'function');
    var rect = new lil.Rect({
      top: 1,
      width: 100,
      height: 100,
      angle: 30,
      scaleX: 2,
      scaleY: 1,
      flipX: true,
      flipY: true,
      skewX: 30,
      skewY: 30
    });
    var transform = lil.util.saveObjectTransform(rect);
    assert.equal(transform.skewX, 30);
    assert.equal(transform.skewY, 30);
    assert.equal(transform.scaleX, 2);
    assert.equal(transform.scaleY, 1);
    assert.equal(transform.flipX, true);
    assert.equal(transform.flipY, true);
    assert.equal(transform.angle, 30);
  });

  QUnit.test('invertTransform', function(assert) {
    assert.ok(typeof lil.util.invertTransform === 'function');
    var m1 = [1, 2, 3, 4, 5, 6], m3;
    m3 = lil.util.invertTransform(m1);
    assert.deepEqual(m3, [-2, 1, 1.5, -0.5, 1, -2]);
  });

  QUnit.test('lil.util.request', function(assert) {
    assert.ok(typeof lil.util.request === 'function', 'lil.util.request is a function');
  });

  QUnit.test('lil.util.getPointer', function(assert) {
    assert.ok(typeof lil.util.getPointer === 'function', 'lil.util.getPointer is a function');
  });

  QUnit.test('rotateVector', function(assert) {
    assert.ok(typeof lil.util.rotateVector === 'function');
  });

  QUnit.test('rotatePoint', function(assert) {
    assert.ok(typeof lil.util.rotatePoint === 'function');
    var origin = new lil.Point(3, 0);
    var point = new lil.Point(4, 0);
    var rotated = lil.util.rotatePoint(point, origin, Math.PI);
    assert.equal(Math.round(rotated.x), 2);
    assert.equal(Math.round(rotated.y), 0);
    var rotated = lil.util.rotatePoint(point, origin, Math.PI / 2);
    assert.equal(Math.round(rotated.x), 3);
    assert.equal(Math.round(rotated.y), -2);
  });

  QUnit.test('transformPoint', function(assert) {
    assert.ok(typeof lil.util.transformPoint === 'function');
    var point = new lil.Point(2, 2);
    var matrix = [3, 0, 0, 2, 10, 4];
    var tp = lil.util.transformPoint(point, matrix);
    assert.equal(Math.round(tp.x), 16);
    assert.equal(Math.round(tp.y), 8);
  });

  QUnit.test('makeBoundingBoxFromPoints', function(assert) {
    assert.ok(typeof lil.util.makeBoundingBoxFromPoints === 'function');
  });

  QUnit.test('parseUnit', function(assert) {
    assert.ok(typeof lil.util.parseUnit === 'function');
    assert.equal(Math.round(lil.util.parseUnit('30mm'), 0), 113, '30mm is pixels');
    assert.equal(Math.round(lil.util.parseUnit('30cm'), 0), 1134, '30cm is pixels');
    assert.equal(Math.round(lil.util.parseUnit('30in'), 0), 2880, '30in is pixels');
    assert.equal(Math.round(lil.util.parseUnit('30pt'), 0), 40, '30mm is pixels');
    assert.equal(Math.round(lil.util.parseUnit('30pc'), 0), 480, '30mm is pixels');
  });

  QUnit.test('createCanvasElement', function(assert) {
    assert.ok(typeof lil.util.createCanvasElement === 'function');
    var element = lil.util.createCanvasElement();
    assert.ok(element.getContext);
  });

  QUnit.test('createImage', function(assert) {
    assert.ok(typeof lil.util.createImage === 'function');
    var element = lil.util.createImage();
    assert.equal(element.naturalHeight, 0);
    assert.equal(element.naturalWidth, 0);
  });

  // QUnit.test('createAccessors', function(assert) {
  //   assert.ok(typeof lil.util.createAccessors === 'function');
  // });

  QUnit.test('qrDecompose with identity matrix', function(assert) {
    assert.ok(typeof lil.util.qrDecompose === 'function');
    var options = lil.util.qrDecompose(lil.iMatrix);
    assert.equal(options.scaleX, 1, 'imatrix has scale 1');
    assert.equal(options.scaleY, 1, 'imatrix has scale 1');
    assert.equal(options.skewX, 0, 'imatrix has skewX 0');
    assert.equal(options.skewY, 0, 'imatrix has skewY 0');
    assert.equal(options.angle, 0, 'imatrix has angle 0');
    assert.equal(options.translateX, 0, 'imatrix has translateX 0');
    assert.equal(options.translateY, 0, 'imatrix has translateY 0');
  });

  QUnit.test('qrDecompose with matrix', function(assert) {
    assert.ok(typeof lil.util.qrDecompose === 'function');
    var options = lil.util.qrDecompose([2, 0.4, 0.5, 3, 100, 200]);
    assert.equal(Math.round(options.scaleX, 4), 2, 'imatrix has scale');
    assert.equal(Math.round(options.scaleY, 4), 3, 'imatrix has scale');
    assert.equal(Math.round(options.skewX, 4), 28, 'imatrix has skewX');
    assert.equal(options.skewY, 0, 'imatrix has skewY 0');
    assert.equal(Math.round(options.angle, 4), 11, 'imatrix has angle 0');
    assert.equal(options.translateX, 100, 'imatrix has translateX 100');
    assert.equal(options.translateY, 200, 'imatrix has translateY 200');
  });

  QUnit.test('composeMatrix with defaults', function(assert) {
    assert.ok(typeof lil.util.composeMatrix === 'function');
    var matrix = lil.util.composeMatrix({
      scaleX: 2,
      scaleY: 3,
      skewX: 28,
      angle: 11,
      translateX: 100,
      translateY: 200,
    }).map(function(val) {
      return lil.util.toFixed(val, 2);
    });
    assert.deepEqual(matrix, [1.96, 0.38, 0.47, 3.15, 100, 200], 'default is identity matrix');
  });

  QUnit.test('composeMatrix with options', function(assert) {
    assert.ok(typeof lil.util.composeMatrix === 'function');
    var matrix = lil.util.composeMatrix({});
    assert.deepEqual(matrix, lil.iMatrix, 'default is identity matrix');
  });

  QUnit.test('drawArc', function(assert) {
    assert.ok(typeof lil.util.drawArc === 'function');
    var canvas = this.canvas = new lil.StaticCanvas(null, {enableRetinaScaling: false, width: 600, height: 600});
    var ctx = canvas.contextContainer;
    lil.util.drawArc(ctx, 0, 0, [
      50,
      30,
      0,
      1,
      1,
      100,
      100,
    ]);
    lil.util.drawArc(ctx, 0, 0, [
      50,
      30,
      0,
      1,
      1,
      100,
      100,
    ]);
  });

  QUnit.test('get bounds of arc', function(assert) {
    assert.ok(typeof lil.util.getBoundsOfArc === 'function');
    var bounds = lil.util.getBoundsOfArc(0, 0, 50, 30, 0, 1, 1, 100, 100);
    var expectedBounds = [
      { x: 0, y: -8.318331151877368 },
      { x: 133.33333333333331, y: 19.99999999999999 },
      { x: 100.00000000000003, y: 19.99999999999999 },
      { x: 147.19721858646224, y: 100 },
    ];
    assert.deepEqual(bounds, expectedBounds, 'bounds are as expected');
  });

  QUnit.test('lil.util.limitDimsByArea', function(assert) {
    assert.ok(typeof lil.util.limitDimsByArea === 'function');
    var dims = lil.util.limitDimsByArea(1, 10000);
    assert.equal(dims.x, 100);
    assert.equal(dims.y, 100);
  });

  QUnit.test('lil.util.limitDimsByArea ar > 1', function(assert) {
    var dims = lil.util.limitDimsByArea(3, 10000);
    assert.equal(dims.x, 173);
    assert.equal(dims.y, 57);
  });

  QUnit.test('lil.util.limitDimsByArea ar < 1', function(assert) {
    var dims = lil.util.limitDimsByArea(1 / 3, 10000);
    assert.equal(dims.x, 57);
    assert.equal(dims.y, 173);
  });

  QUnit.test('lil.util.capValue ar < 1', function(assert) {
    assert.ok(typeof lil.util.capValue === 'function');
    var val = lil.util.capValue(3, 10, 70);
    assert.equal(val, 10, 'value is not capped');
  });

  QUnit.test('lil.util.capValue ar < 1', function(assert) {
    assert.ok(typeof lil.util.capValue === 'function');
    var val = lil.util.capValue(3, 1, 70);
    assert.equal(val, 3, 'min cap');
  });

  QUnit.test('lil.util.capValue ar < 1', function(assert) {
    assert.ok(typeof lil.util.capValue === 'function');
    var val = lil.util.capValue(3, 80, 70);
    assert.equal(val, 70, 'max cap');
  });

  QUnit.test('lil.util.cos', function(assert) {
    assert.ok(typeof lil.util.cos === 'function');
    assert.equal(lil.util.cos(0), 1, 'cos 0 correct');
    assert.equal(lil.util.cos(Math.PI / 2), 0, 'cos 90 correct');
    assert.equal(lil.util.cos(Math.PI), -1, 'cos 180 correct');
    assert.equal(lil.util.cos(3 * Math.PI / 2), 0,' cos 270 correct');
  });

  QUnit.test('lil.util.getSvgAttributes', function(assert) {
    assert.ok(typeof lil.util.getSvgAttributes === 'function');
    assert.deepEqual(lil.util.getSvgAttributes(''),
      ['instantiated_by_use', 'style', 'id', 'class'], 'common attribs');
    assert.deepEqual(lil.util.getSvgAttributes('linearGradient'),
      ['instantiated_by_use', 'style', 'id', 'class', 'x1', 'y1', 'x2', 'y2', 'gradientUnits', 'gradientTransform'],
      'linearGradient attribs');
    assert.deepEqual(lil.util.getSvgAttributes('radialGradient'),
      ['instantiated_by_use', 'style', 'id', 'class', 'gradientUnits', 'gradientTransform', 'cx', 'cy', 'r', 'fx', 'fy', 'fr'],
      'radialGradient attribs');
    assert.deepEqual(lil.util.getSvgAttributes('stop'),
      ['instantiated_by_use', 'style', 'id', 'class', 'offset', 'stop-color', 'stop-opacity'],
      'stop attribs');
  });

  QUnit.test('lil.util.enlivenPatterns', function(assert) {
    assert.ok(typeof lil.util.enlivenPatterns === 'function');
    lil.util.enlivenPatterns([], function() {
      assert.ok(true, 'callBack is called when no patterns are available');
    });
  });

  QUnit.test('lil.util.copyCanvasElement', function(assert) {
    assert.ok(typeof lil.util.copyCanvasElement === 'function');
    var c = lil.util.createCanvasElement();
    c.width = 10;
    c.height = 20;
    c.getContext('2d').fillStyle = 'red';
    c.getContext('2d').fillRect(0, 0, 10, 10);
    var b = lil.util.copyCanvasElement(c);
    assert.equal(b.width, 10, 'width has been copied');
    assert.equal(b.height, 20, 'height has been copied');
    var data = b.getContext('2d').getImageData(1,1,1,1).data;
    assert.equal(data[0], 255, 'red color has been copied');
    assert.equal(data[1], 0, 'red color has been copied');
    assert.equal(data[2], 0, 'red color has been copied');
    assert.equal(data[3], 255, 'red color has been copied');
  });

  QUnit.test('lil.util.findScaleToCover', function(assert) {
    assert.ok(typeof lil.util.findScaleToCover === 'function');
    var scale = lil.util.findScaleToCover({
      width: 100,
      height: 200,
    }, {
      width: 50,
      height: 50,
    });
    assert.equal(scale, 0.5, 'scaleToCover is 0.5');
    var scale = lil.util.findScaleToCover({
      width: 10,
      height: 25,
    }, {
      width: 50,
      height: 50,
    });
    assert.equal(scale, 5, 'scaleToCover is 5');
  });

  QUnit.test('lil.util.findScaleToFit', function(assert) {
    assert.ok(typeof lil.util.findScaleToFit === 'function');
    var scale = lil.util.findScaleToFit({
      width: 100,
      height: 200,
    }, {
      width: 50,
      height: 50,
    });
    assert.equal(scale, 0.25, 'findScaleToFit is 0.25');
    var scale = lil.util.findScaleToFit({
      width: 10,
      height: 25,
    }, {
      width: 50,
      height: 50,
    });
    assert.equal(scale, 2, 'findScaleToFit is 2');
  });

  QUnit.test('lil.util.isTouchEvent', function(assert) {
    assert.ok(typeof lil.util.isTouchEvent === 'function');
    assert.ok(lil.util.isTouchEvent({ type: 'touchstart' }));
    assert.ok(lil.util.isTouchEvent({ type: 'touchend' }));
    assert.ok(lil.util.isTouchEvent({ type: 'touchmove' }));
    assert.ok(lil.util.isTouchEvent({ pointerType: 'touch' }));
    assert.notOk(lil.util.isTouchEvent({ type: 'mousedown' }));
    assert.notOk(lil.util.isTouchEvent({ pointerType: 'mouse' }));
  });
})();
