(function(){

  // var canvas = this.canvas = new lil.StaticCanvas(null, {enableRetinaScaling: false});

  QUnit.module('lil.Object - clipPath', {
    afterEach: function() {
      // canvas.clear();
      // canvas.calcOffset();
    }
  });

  QUnit.test('constructor & properties', function(assert) {
    var cObj = new lil.Object();
    assert.equal(cObj.clipPath, undefined, 'clipPath should not be defined out of the box');
  });

  QUnit.test('toObject with clipPath', function(assert) {
    var emptyObjectRepr = {
      version:                  lil.version,
      type:                     'object',
      originX:                  'left',
      originY:                  'top',
      left:                     0,
      top:                      0,
      width:                    0,
      height:                   0,
      fill:                     'rgb(0,0,0)',
      stroke:                   null,
      strokeWidth:              1,
      strokeDashArray:          null,
      strokeLineCap:            'butt',
      strokeDashOffset:         0,
      strokeLineJoin:           'miter',
      strokeMiterLimit:         4,
      scaleX:                   1,
      scaleY:                   1,
      angle:                    0,
      flipX:                    false,
      flipY:                    false,
      opacity:                  1,
      shadow:                   null,
      visible:                  true,
      backgroundColor:          '',
      clipTo:                   null,
      fillRule:                 'nonzero',
      paintFirst:               'fill',
      globalCompositeOperation: 'source-over',
      skewX:                      0,
      skewY:                      0,
      transformMatrix:          null,
    };

    var cObj = new lil.Object();
    assert.deepEqual(emptyObjectRepr, cObj.toObject());

    cObj.clipPath = new lil.Object();
    var expected = lil.util.object.clone(emptyObjectRepr);
    var expectedClipPath = lil.util.object.clone(emptyObjectRepr);
    expectedClipPath = lil.util.object.extend(expectedClipPath, {
      inverted: cObj.clipPath.inverted,
      absolutePositioned: cObj.clipPath.absolutePositioned,
    });
    expected.clipPath = expectedClipPath;
    assert.deepEqual(expected, cObj.toObject());
  });

  QUnit.test('from object with clipPath', function(assert) {
    var done = assert.async();
    var rect = new lil.Rect({ width: 100, height: 100 });
    rect.clipPath = new lil.Circle({ radius: 50 });
    var toObject = rect.toObject();
    lil.Rect.fromObject(toObject, function(rect) {
      assert.ok(rect.clipPath instanceof lil.Circle, 'clipPath is enlived');
      assert.equal(rect.clipPath.radius, 50, 'radius is restored correctly');
      done();
    });
  });

  QUnit.test('from object with clipPath inverted, absolutePositioned', function(assert) {
    var done = assert.async();
    var rect = new lil.Rect({ width: 100, height: 100 });
    rect.clipPath = new lil.Circle({ radius: 50, inverted: true, absolutePositioned: true });
    var toObject = rect.toObject();
    lil.Rect.fromObject(toObject, function(rect) {
      assert.ok(rect.clipPath instanceof lil.Circle, 'clipPath is enlived');
      assert.equal(rect.clipPath.radius, 50, 'radius is restored correctly');
      assert.equal(rect.clipPath.inverted, true, 'inverted is restored correctly');
      assert.equal(rect.clipPath.absolutePositioned, true, 'absolutePositioned is restored correctly');
      done();
    });
  });

  QUnit.test('from object with clipPath, nested', function(assert) {
    var done = assert.async();
    var rect = new lil.Rect({ width: 100, height: 100 });
    rect.clipPath = new lil.Circle({ radius: 50 });
    rect.clipPath.clipPath = new lil.Text('clipPath');
    var toObject = rect.toObject();
    lil.Rect.fromObject(toObject, function(rect) {
      assert.ok(rect.clipPath instanceof lil.Circle, 'clipPath is enlived');
      assert.equal(rect.clipPath.radius, 50, 'radius is restored correctly');
      assert.ok(rect.clipPath.clipPath instanceof lil.Text, 'neted clipPath is enlived');
      assert.equal(rect.clipPath.clipPath.text, 'clipPath', 'instance is restored correctly');
      done();
    });
  });

  QUnit.test('from object with clipPath, nested inverted, absolutePositioned', function(assert) {
    var done = assert.async();
    var rect = new lil.Rect({ width: 100, height: 100 });
    rect.clipPath = new lil.Circle({ radius: 50 });
    rect.clipPath.clipPath = new lil.Text('clipPath', { inverted: true, absolutePositioned: true});
    var toObject = rect.toObject();
    lil.Rect.fromObject(toObject, function(rect) {
      assert.ok(rect.clipPath instanceof lil.Circle, 'clipPath is enlived');
      assert.equal(rect.clipPath.radius, 50, 'radius is restored correctly');
      assert.ok(rect.clipPath.clipPath instanceof lil.Text, 'neted clipPath is enlived');
      assert.equal(rect.clipPath.clipPath.text, 'clipPath', 'instance is restored correctly');
      assert.equal(rect.clipPath.clipPath.inverted, true, 'instance inverted is restored correctly');
      assert.equal(rect.clipPath.clipPath.absolutePositioned, true, 'instance absolutePositioned is restored correctly');
      done();
    });
  });

  QUnit.test('_setClippingProperties fix the context props', function(assert) {
    var canvas = new lil.Canvas();
    var rect = new lil.Rect({ width: 100, height: 100 });
    canvas.contextContainer.fillStyle = 'red';
    canvas.contextContainer.strokeStyle = 'blue';
    canvas.contextContainer.globalAlpha = 0.3;
    rect._setClippingProperties(canvas.contextContainer);
    assert.equal(canvas.contextContainer.fillStyle, '#000000', 'fillStyle is reset');
    assert.equal(new lil.Color(canvas.contextContainer.strokeStyle).getAlpha(), 0, 'stroke style is reset');
    assert.equal(canvas.contextContainer.globalAlpha, 1, 'globalAlpha is reset');
  });

  QUnit.test('clipPath caching detection', function(assert) {
    var cObj = new lil.Object();
    var clipPath = new lil.Object();
    cObj.statefullCache = true;
    cObj.saveState({ propertySet: 'cacheProperties' });
    var change = cObj.hasStateChanged('cacheProperties');
    assert.equal(change, false, 'cache is clean');

    cObj.clipPath = clipPath;
    change = cObj.hasStateChanged('cacheProperties');
    assert.equal(change, true, 'cache is dirty');

    cObj.saveState({ propertySet: 'cacheProperties' });

    change = cObj.hasStateChanged('cacheProperties');
    assert.equal(change, false, 'cache is clean again');

    cObj.clipPath.fill = 'red';
    change = cObj.hasStateChanged('cacheProperties');
    assert.equal(change, true, 'cache change in clipPath is detected');
  });

  QUnit.test('clipPath caching detection with canvas object', function(assert) {
    var canvas = new lil.StaticCanvas(null, { renderOnAddRemove: false });
    var cObj = new lil.Rect();
    var clipPath = new lil.Rect();
    canvas.add(cObj);
    clipPath.canvas = canvas;
    cObj.statefullCache = true;
    cObj.saveState({ propertySet: 'cacheProperties' });
    var change = cObj.hasStateChanged('cacheProperties');
    assert.equal(change, false, 'cache is clean - canvas');

    cObj.clipPath = clipPath;
    change = cObj.hasStateChanged('cacheProperties');
    assert.equal(change, true, 'cache is dirty - canvas');

    cObj.saveState({ propertySet: 'cacheProperties' });

    change = cObj.hasStateChanged('cacheProperties');
    assert.equal(change, false, 'cache is clean again - canvas');

    cObj.clipPath.fill = 'red';
    change = cObj.hasStateChanged('cacheProperties');
    assert.equal(change, true, 'cache change in clipPath is detected - canvas');
  });
})();
