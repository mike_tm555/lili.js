(function() {

  QUnit.module('lil.Intersection');

  QUnit.test('constructor & properties', function(assert) {
    assert.ok(typeof lil.Intersection === 'function');

    var intersection = new lil.Intersection();

    assert.ok(intersection);
    assert.ok(intersection instanceof lil.Intersection);
    assert.ok(intersection.constructor === lil.Intersection);
    assert.ok(typeof intersection.constructor === 'function');
    assert.deepEqual(intersection.points, [], 'starts with empty array of points');
    assert.ok('status' in intersection, 'has status property');
    assert.equal(intersection.status, undefined, 'no default value for status');

    var status = 'status';
    intersection = new lil.Intersection(status);
    assert.equal(intersection.status, status, 'constructor pass status value');
  });

  QUnit.test('appendPoint', function(assert) {
    var point = new lil.Point(1, 1);
    var intersection = new lil.Intersection();
    assert.ok(typeof intersection.appendPoint === 'function', 'has appendPoint method');
    var returned = intersection.appendPoint(point);
    assert.ok(returned instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(returned, intersection, 'is chainable');
    assert.equal(intersection.points.indexOf(point), 0, 'now intersection contain points');
  });

  QUnit.test('appendPoints', function(assert) {
    var point = new lil.Point(1, 1);
    var intersection = new lil.Intersection();
    assert.ok(typeof intersection.appendPoints === 'function', 'has appendPoint method');
    var returned = intersection.appendPoints([point, point]);
    assert.ok(returned instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(returned, intersection, 'is chainable');
    assert.equal(intersection.points.indexOf(point), 0, 'now intersection contain points');
    assert.equal(intersection.points.length, 2, 'now intersection contains 2 points');
  });

  QUnit.test('intersectLineLine simple intersection', function(assert) {
    var p1 = new lil.Point(0, 0), p2 = new lil.Point(10,10),
        p3 = new lil.Point(0, 10), p4 = new lil.Point(10, 0),
        intersection = lil.Intersection.intersectLineLine(p1, p2, p3, p4);
    assert.ok(typeof lil.Intersection.intersectLineLine === 'function', 'has intersectLineLine function');
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(intersection.status, 'Intersection', 'it return a inteserction result');
    assert.deepEqual(intersection.points[0], new lil.Point(5, 5), 'intersect in 5,5');
  });

  QUnit.test('intersectLineLine parallel', function(assert) {
    var p1 = new lil.Point(0, 0), p2 = new lil.Point(0,10),
        p3 = new lil.Point(10, 0), p4 = new lil.Point(10, 10),
        intersection = lil.Intersection.intersectLineLine(p1, p2, p3, p4);
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(intersection.status, 'Parallel', 'it return a Parallel result');
    assert.deepEqual(intersection.points, [], 'no point of intersections');
  });

  QUnit.test('intersectLineLine coincident', function(assert) {
    var p1 = new lil.Point(0, 0), p2 = new lil.Point(0, 10),
        p3 = new lil.Point(0, 0), p4 = new lil.Point(0, 10),
        intersection = lil.Intersection.intersectLineLine(p1, p2, p3, p4);
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(intersection.status, 'Coincident', 'it return a Coincident result');
    assert.deepEqual(intersection.points, [], 'no point of intersections');
  });

  QUnit.test('intersectLineLine coincident but different', function(assert) {
    var p1 = new lil.Point(0, 0), p2 = new lil.Point(0, 10),
        p3 = new lil.Point(0, 1), p4 = new lil.Point(0, 9),
        intersection = lil.Intersection.intersectLineLine(p1, p2, p3, p4);
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(intersection.status, 'Coincident', 'it return a Coincident result');
    assert.deepEqual(intersection.points, [], 'no point of intersections');
  });

  QUnit.test('intersectLineLine no intersect', function(assert) {
    var p1 = new lil.Point(0, 0), p2 = new lil.Point(0,10),
        p3 = new lil.Point(10, 0), p4 = new lil.Point(1, 10),
        intersection = lil.Intersection.intersectLineLine(p1, p2, p3, p4);
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(intersection.status, undefined, 'it return a undefined status result');
    assert.deepEqual(intersection.points, [], 'no point of intersections');
  });

  QUnit.test('intersectLinePolygon', function(assert) {
    var p1 = new lil.Point(0, 5), p2 = new lil.Point(10, 5),
        p3 = new lil.Point(5, 0), p4 = new lil.Point(2, 10),
        p5 = new lil.Point(8, 10), points = [p3, p4, p5],
        intersection = lil.Intersection.intersectLinePolygon(p1, p2, points);
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.ok(typeof lil.Intersection.intersectLinePolygon === 'function', 'has intersectLinePolygon function');
    assert.equal(intersection.status, 'Intersection', 'it return a Intersection result');
    assert.equal(intersection.points.length, 2, '2 points of intersections');
    assert.deepEqual(intersection.points[0], new lil.Point(3.5, 5), 'intersect in 3.5 ,5');
    assert.deepEqual(intersection.points[1], new lil.Point(6.5, 5), 'intersect in 6.5 ,5');
  });

  QUnit.test('intersectLinePolygon in one point', function(assert) {
    var p1 = new lil.Point(0, 5), p2 = new lil.Point(5, 5),
        p3 = new lil.Point(5, 0), p4 = new lil.Point(2, 10),
        p5 = new lil.Point(8, 10), points = [p3, p4, p5],
        intersection = lil.Intersection.intersectLinePolygon(p1, p2, points);
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(intersection.status, 'Intersection', 'it return a Intersection result');
    assert.equal(intersection.points.length, 1, '1 points of intersections');
    assert.deepEqual(intersection.points[0], new lil.Point(3.5, 5), 'intersect in 3.5 ,5');
  });

  QUnit.test('intersectLinePolygon in one point', function(assert) {
    var p1 = new lil.Point(0, 5), p2 = new lil.Point(3, 5),
        p3 = new lil.Point(5, 0), p4 = new lil.Point(2, 10),
        p5 = new lil.Point(8, 10), points = [p3, p4, p5],
        intersection = lil.Intersection.intersectLinePolygon(p1, p2, points);
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(intersection.status, undefined, 'it return a undefined result');
    assert.equal(intersection.points.length, 0, '0 points of intersections');
  });

  QUnit.test('intersectLinePolygon on a polygon segment', function(assert) {
    //TODO: fix this. it should return coincident.
    var p1 = new lil.Point(1, 10), p2 = new lil.Point(9, 10),
        p3 = new lil.Point(5, 0), p4 = new lil.Point(2, 10),
        p5 = new lil.Point(8, 10), points = [p3, p4, p5],
        intersection = lil.Intersection.intersectLinePolygon(p1, p2, points);
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(intersection.status, 'Intersection', 'it return a Intersection result');
    assert.equal(intersection.points.length, 2, '2 points of intersections');
    assert.deepEqual(intersection.points[0], new lil.Point(2, 10), 'intersect in 2, 10');
    assert.deepEqual(intersection.points[1], new lil.Point(8, 10), 'intersect in 8, 10');
  });

  QUnit.test('intersectPolygonPolygon not intersecting', function(assert) {
    var p3b = new lil.Point(50, 0), p4b = new lil.Point(20, 100),
        p5b = new lil.Point(80, 100), pointsb = [p3b, p4b, p5b],
        p3 = new lil.Point(5, 0), p4 = new lil.Point(2, 10),
        p5 = new lil.Point(8, 10), points = [p3, p4, p5],
        intersection = lil.Intersection.intersectPolygonPolygon(pointsb, points);
    assert.ok(typeof lil.Intersection.intersectPolygonPolygon === 'function', 'has intersectPolygonPolygon function');
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(intersection.status, undefined, 'it return a Intersection with no status');
    assert.equal(intersection.points.length, 0, '0 points of intersections');
  });

  QUnit.test('intersectPolygonPolygon intersecting', function(assert) {
    var p3b = new lil.Point(1, 1), p4b = new lil.Point(3, 1),
        p5b = new lil.Point(3, 3), p6b = new lil.Point(1, 3),
        pointsb = [p3b, p4b, p5b, p6b],
        p3 = new lil.Point(2, 2), p4 = new lil.Point(4, 2),
        p5 = new lil.Point(4, 4), p6 = new lil.Point(2, 4),
        points = [p3, p4, p5, p6],
        intersection = lil.Intersection.intersectPolygonPolygon(pointsb, points);
    assert.ok(typeof lil.Intersection.intersectPolygonPolygon === 'function', 'has intersectPolygonPolygon function');
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(intersection.status, 'Intersection', 'it return a Intersection result');
    assert.equal(intersection.points.length, 2, '2 points of intersections');
    assert.deepEqual(intersection.points[0], new lil.Point(3, 2), 'point of intersections 3, 2');
    assert.deepEqual(intersection.points[1], new lil.Point(2, 3), 'point of intersections 2, 3');
  });

  QUnit.test('intersectPolygonRectangle intersecting', function(assert) {
    var p3b = new lil.Point(1, 1),
        p5b = new lil.Point(3, 3),
        p3 = new lil.Point(2, 2), p4 = new lil.Point(4, 2),
        p5 = new lil.Point(4, 4), p6 = new lil.Point(2, 4),
        points = [p3, p4, p5, p6],
        intersection = lil.Intersection.intersectPolygonRectangle(points, p3b, p5b);
    assert.ok(typeof lil.Intersection.intersectPolygonRectangle === 'function', 'has intersectPolygonPolygon function');
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(intersection.status, 'Intersection', 'it return a Intersection result');
    assert.equal(intersection.points.length, 2, '2 points of intersections');
    assert.deepEqual(intersection.points[0], new lil.Point(3, 2), 'point of intersections 3, 2');
    assert.deepEqual(intersection.points[1], new lil.Point(2, 3), 'point of intersections 2, 3');
  });

  QUnit.test('intersectPolygonRectangle not intersecting', function(assert) {
    var p3b = new lil.Point(10, 10),
        p5b = new lil.Point(30, 30),
        p3 = new lil.Point(2, 2), p4 = new lil.Point(4, 2),
        p5 = new lil.Point(4, 4), p6 = new lil.Point(2, 4),
        points = [p3, p4, p5, p6],
        intersection = lil.Intersection.intersectPolygonRectangle(points, p3b, p5b);
    assert.ok(typeof lil.Intersection.intersectPolygonRectangle === 'function', 'has intersectPolygonPolygon function');
    assert.ok(intersection instanceof lil.Intersection, 'returns a lil.Intersection');
    assert.equal(intersection.status, undefined, 'it return a Intersection result');
    assert.equal(intersection.points.length, 0, '0 points of intersections');
  });
})();
