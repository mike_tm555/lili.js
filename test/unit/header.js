(function() {
  QUnit.module('lil header.js');

  QUnit.test('default values', function(assert) {
    assert.ok(typeof lil.document !== 'undefined', 'document is set');
    assert.ok(typeof lil.window !== 'undefined', 'window is set');
    assert.ok(typeof lil.isTouchSupported !== 'undefined', 'isTouchSupported is set');
    assert.ok(typeof lil.isLikelyNode !== 'undefined', 'isLikelyNode is set');
    assert.equal(lil.SHARED_ATTRIBUTES.length, 19, 'SHARED_ATTRIBUTES is set');
  });

  QUnit.test('initFilterBackend', function(assert) {
    assert.ok(typeof lil.initFilterBackend === 'function', 'initFilterBackend is a function');
    assert.ok(typeof lil.maxTextureSize === 'undefined', 'maxTextureSize is not set yet');
    lil.initFilterBackend();
  });
})();
