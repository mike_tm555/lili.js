(function(window) {
  function visualCallback() {
    this.currentArgs = {};
  }

  visualCallback.prototype.addArguments = function(argumentObj) {
    this.currentArgs = {
      enabled: true,
      lil: argumentObj.lil,
      golden: argumentObj.golden,
      diff: argumentObj.diff,
    };
  };

  visualCallback.prototype.testDone = function(details) {
    if (window && document && this.currentArgs.enabled) {
      var lilCanvas = this.currentArgs.lil;
      var ouputImageDataRef = this.currentArgs.diff;
      var goldenCanvasRef = this.currentArgs.golden;
      var id = 'qunit-test-output-' + details.testId;
      var node = document.getElementById(id);
      var lilCopy = document.createElement('canvas');
      var diff = document.createElement('canvas');
      diff.width = lilCopy.width = lilCanvas.width;
      diff.height = lilCopy.height = lilCanvas.height;
      diff.getContext('2d').putImageData(ouputImageDataRef, 0, 0);
      lilCopy.getContext('2d').drawImage(lilCanvas, 0, 0);
      var _div = document.createElement('div');
      _div.appendChild(goldenCanvasRef);
      _div.appendChild(lilCopy);
      _div.appendChild(diff);
      node.appendChild(_div);
      // after one run, disable
      this.currentArgs.enabled = false;
    }
  };

  if (window) {
    window.visualCallback = new visualCallback();
  }
})(this);
