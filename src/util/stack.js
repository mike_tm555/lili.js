(function() {
  function iniStack(canvas) {
    //variables for undo/redo
    var pauseSaving = false;
    var undoStack = [];
    var redoStack = [];

    canvas.on('object:added', function () {
      if (!pauseSaving) {
        undoStack.push(canvas.toJSON());
        redoStack = [];
      }
    });

    canvas.on('object:modified', function () {
      if (!pauseSaving) {
        undoStack.push(canvas.toJSON());
        redoStack = [];
      }
    });

    canvas.on('object:removed', function () {
      if (!pauseSaving) {
        undoStack.push(canvas.toJSON());
        redoStack = [];
      }
    });

    canvas.undo = function () {
      pauseSaving = true;
      redoStack.push(undoStack.pop());
      var previousState = undoStack[undoStack.length - 1];
      if (!previousState) {
        previousState = '{}';
      }
      canvas.loadFromJSON(previousState).renderAll();
      pauseSaving = false;
    };

    canvas.redo = function () {
      pauseSaving = true;
      var state = redoStack.pop();
      if (state) {
        undoStack.push(state);
        canvas.loadFromJSON(state).renderAll();
        pauseSaving = false;
      }
    };
    canvas.current = function () {
      return canvas.toJSON();
    };
  }
  lil.util.initStack = iniStack;
})();
