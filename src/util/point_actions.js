(function() {
  function generateCircle(_polyPoints, activeObject, callBack) {
    var linesIds = [];
    var lines = [];
    var id = 0;
    var options = {
      fill: 'transparent',
      stroke: 'blue',
      type: 'line',
      strokeWidth: 10,
      perPixelTargetFind: false,
      hasControls: false,
      hasBorders: false,
      lockMovementX: true,
      lockMovementY: true,
      lockScalingX: true,
      lockScalingY: true,
      lockRotation: true,
      originX: 'center',
      originY: 'center',
      prevPoint: 0
    };

    for (var i = 0; i < _polyPoints.length; i++) {
      var start = _polyPoints[i];
      var end = _polyPoints[i + 1];
      var line = null;
      var circle = null;

      options.prevPoint = i;

      if (end) {
        line = new line.Line([start.x, start.y, end.x, end.y], options);
      }
      else {
        line = new line.Line([start.x, start.y, _polyPoints[0].x, _polyPoints[0].y], options);
      }

      circle = new line.Circle({
        left: start.x,
        top: start.y,
        originX: 'center',
        originY: 'center',
        hasControls: false,
        hasBorders: false,
        radius: 10,
        fill: 'red',
        type: 'point',
      });

      line.id = id++;
      lines.push(line);
      linesIds.push(line.id);

      circle.id = id++;
      circle.index = i;

      circle.referenceId = {
        start: line.id,
        end: line.id - 2
      };

      circle.linesIds = linesIds;

      circle.on('moving', function () {
        lil.canvas.getObjectById(this.referenceId.start).set({
          x1: this.left,
          y1: this.top
        }).setCoords();

        var endId = (this.referenceId.end < 0) ? this.linesIds[this.linesIds.length - 1] : this.referenceId.end;

        var currentPoint = {
          x: this.left,
          y: this.top
        };

        lil.canvas.getObjectById(endId).set({
          x2: currentPoint.x,
          y2: currentPoint.y
        }).setCoords();

        activeObject.points[this.index] = {
          x: currentPoint.x,
          y: currentPoint.y
        };

        activeObject.points = activeObject.points.map(function (point) {
          return {
            x: point.x,
            y: point.y,
          };
        });

        if (typeof callBack === 'function') {
          callBack(activeObject);
        }
      });

      lines.push(circle);
    }

    return lines;
  }

  function recalculatePoints(obj) {
    var matrix = obj.calcTransformMatrix();
    var points = obj.points.map(function(p){
      var pointX = p.x - obj.minX - obj.width / 2;
      var pointY = p.y - obj.minY - obj.height / 2;
      return new lil.Point(pointX, pointY);
    }).map(function(p) {
      return lil.util.transformPoint(p, matrix);
    }).map(function(point) {
      return new lil.Point(point.x, point.y);
    });

    obj._calcDimensions();

    return points;
  }

  function getObjectPartials(object) {
    var options = {
      fill: 'transparent',
      stroke: 'blue',
      type: 'line',
      strokeWidth: 10,
      perPixelTargetFind: false,
      hasControls: false,
      hasBorders: false,
      lockMovementX: true,
      lockMovementY: true,
      lockScalingX: true,
      lockScalingY: true,
      lockRotation: true,
      originX: 'center',
      originY: 'center',
      prevPoint: 0
    };
    var points = [];
    var lines = [];
    var lineIds = [];
    var id = 0;
    for (var i = 0; i < object.points.length; i++) {
      var start = object.points[i];
      var end = object.points[i + 1];
      var line = null;
      var circle = null;

      options.prevPoint = i;

      if (end) {
        line = new lil.Line([start.x, start.y, end.x, end.y], options);
      }
      else {
        line = new lil.Line([start.x, start.y, object.points[0].x, object.points[0].y], options);
      }

      line.id = id++;
      lines.push(line);
      lineIds.push(line.id);
      circle = new lil.Circle({
        left: start.x,
        top: start.y,
        originX: 'center',
        originY: 'center',
        hasControls: false,
        hasBorders: false,
        radius: 5,
        fill: 'red',
        type: 'circle',
      });
      circle.id = id++;
      circle.index = i;
      circle.referenceId = {
        start: line.id,
        end: line.id - 2
      };

      circle.linesIds = lineIds;
      points.push(circle);
    }

    return {
      points: points,
      lines: lines,
    };
  }

  function switchLinesMode(activeObjects, canvas, callBack) {
    activeObjects.map(function (object) {
      if (object.points) {
        object.points = recalculatePoints(object);
        var partials = getObjectPartials(object);
        partials.points.forEach(function (partial) {
          partial.on('moving', function () {
            canvas.getObjectById(this.referenceId.start).set({
              x1: this.left,
              y1: this.top
            }).setCoords();

            var endId = (this.referenceId.end < 0) ? this.linesIds[this.linesIds.length - 1] : this.referenceId.end;
            var currentPoint = {
              x: this.left,
              y: this.top
            };

            canvas.getObjectById(endId).set({
              x2: currentPoint.x,
              y2: currentPoint.y
            }).setCoords();

            object.points[this.index] = {
              x: currentPoint.x,
              y: currentPoint.y
            };

            object.points = object.points.map(function (point) {
              return {
                x: point.x,
                y: point.y,
              };
            });

            object._calcDimensions();

            if (typeof callBack === 'function') {
              callBack(object);
            }
          });
        });
        partials.lines.forEach(function (partial) {
          partial.on('mousedown', function (e) {
            var newPoints = [];

            object.points.forEach(function (point, index) {
              newPoints.push({
                x: point.x,
                y: point.y,
              });

              if (e.target.prevPoint === index) {
                newPoints.push(e.absolutePointer);
              }
            });

            object.points = newPoints;
            object.points = recalculatePoints(object);

            partials.lines.forEach(function (partial) {
              canvas.remove(partial);
            });
            partials.points.forEach(function (partial) {
              canvas.remove(partial);
            });
            object._calcDimensions();
            switchLinesMode(activeObjects, canvas, callBack);
            if (typeof callBack === 'function') {
              callBack(object);
            }
          });
        });

        canvas.remove(object);
        canvas.add.apply(
          canvas,
          partials.lines.concat(partials.points)
        ).renderAll();
      }
    });
  }

  lil.util.generateCircle = generateCircle;
  lil.util.recalculatePoints = recalculatePoints;
  lil.util.switchLinesMode = switchLinesMode;
})();
