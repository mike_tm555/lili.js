(function() {

  /**
   * Creates accessors (getXXX, setXXX) for a "class", based on "stateProperties" array
   * @static
   * @memberOf lil.util
   * @param {Object} klass "Class" to create accessors for
   */
  lil.util.createAccessors = function(klass) {
    var proto = klass.prototype, i, propName,
        capitalizedPropName, setterName, getterName;

    for (i = proto.stateProperties.length; i--; ) {

      propName = proto.stateProperties[i];
      capitalizedPropName = propName.charAt(0).toUpperCase() + propName.slice(1);
      setterName = 'set' + capitalizedPropName;
      getterName = 'get' + capitalizedPropName;

      // using `new Function` for better introspection
      if (!proto[getterName]) {
        proto[getterName] = (function(property) {
          return new Function('return this.get("' + property + '")');
        })(propName);
      }
      if (!proto[setterName]) {
        proto[setterName] = (function(property) {
          return new Function('value', 'return this.set("' + property + '", value)');
        })(propName);
      }
    }
  };

  /** @lends lil.Text.Prototype */
  /**
   * Retrieves object's fontSize
   * @method getFontSize
   * @memberOf lil.Text.prototype
   * @return {String} Font size (in pixels)
   */

  /**
   * Sets object's fontSize
   * Does not update the object .width and .height,
   * call .initDimensions() to update the values.
   * @method setFontSize
   * @memberOf lil.Text.prototype
   * @param {Number} fontSize Font size (in pixels)
   * @return {lil.Text}
   * @chainable
   */

  /**
   * Retrieves object's fontWeight
   * @method getFontWeight
   * @memberOf lil.Text.prototype
   * @return {(String|Number)} Font weight
   */

  /**
   * Sets object's fontWeight
   * Does not update the object .width and .height,
   * call .initDimensions() to update the values.
   * @method setFontWeight
   * @memberOf lil.Text.prototype
   * @param {(Number|String)} fontWeight Font weight
   * @return {lil.Text}
   * @chainable
   */

  /**
   * Retrieves object's fontFamily
   * @method getFontFamily
   * @memberOf lil.Text.prototype
   * @return {String} Font family
   */

  /**
   * Sets object's fontFamily
   * Does not update the object .width and .height,
   * call .initDimensions() to update the values.
   * @method setFontFamily
   * @memberOf lil.Text.prototype
   * @param {String} fontFamily Font family
   * @return {lil.Text}
   * @chainable
   */

  /**
   * Retrieves object's text
   * @method getText
   * @memberOf lil.Text.prototype
   * @return {String} text
   */

  /**
   * Sets object's text
   * Does not update the object .width and .height,
   * call .initDimensions() to update the values.
   * @method setText
   * @memberOf lil.Text.prototype
   * @param {String} text Text
   * @return {lil.Text}
   * @chainable
   */

  /**
   * Retrieves object's underline
   * @method getUnderline
   * @memberOf lil.Text.prototype
   * @return {Boolean} underline enabled or disabled
   */

  /**
   * Sets object's underline
   * @method setUnderline
   * @memberOf lil.Text.prototype
   * @param {Boolean} underline Text decoration
   * @return {lil.Text}
   * @chainable
   */

  /**
   * Retrieves object's fontStyle
   * @method getFontStyle
   * @memberOf lil.Text.prototype
   * @return {String} Font style
   */

  /**
   * Sets object's fontStyle
   * Does not update the object .width and .height,
   * call .initDimensions() to update the values.
   * @method setFontStyle
   * @memberOf lil.Text.prototype
   * @param {String} fontStyle Font style
   * @return {lil.Text}
   * @chainable
   */

  /**
   * Retrieves object's lineHeight
   * @method getLineHeight
   * @memberOf lil.Text.prototype
   * @return {Number} Line height
   */

  /**
   * Sets object's lineHeight
   * @method setLineHeight
   * @memberOf lil.Text.prototype
   * @param {Number} lineHeight Line height
   * @return {lil.Text}
   * @chainable
   */

  /**
   * Retrieves object's textAlign
   * @method getTextAlign
   * @memberOf lil.Text.prototype
   * @return {String} Text alignment
   */

  /**
   * Sets object's textAlign
   * @method setTextAlign
   * @memberOf lil.Text.prototype
   * @param {String} textAlign Text alignment
   * @return {lil.Text}
   * @chainable
   */

  /**
   * Retrieves object's textBackgroundColor
   * @method getTextBackgroundColor
   * @memberOf lil.Text.prototype
   * @return {String} Text background color
   */

  /**
   * Sets object's textBackgroundColor
   * @method setTextBackgroundColor
   * @memberOf lil.Text.prototype
   * @param {String} textBackgroundColor Text background color
   * @return {lil.Text}
   * @chainable
   */

  /** @lends lil.Object.Prototype */
  /**
   * Retrieves object's {@link lil.Object#clipTo|clipping function}
   * @method getClipTo
   * @memberOf lil.Object.prototype
   * @return {Function}
   */

  /**
   * Sets object's {@link lil.Object#clipTo|clipping function}
   * @method setClipTo
   * @memberOf lil.Object.prototype
   * @param {Function} clipTo Clipping function
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#transformMatrix|transformMatrix}
   * @method getTransformMatrix
   * @memberOf lil.Object.prototype
   * @return {Array} transformMatrix
   */

  /**
   * Sets object's {@link lil.Object#transformMatrix|transformMatrix}
   * @method setTransformMatrix
   * @memberOf lil.Object.prototype
   * @param {Array} transformMatrix
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#visible|visible} state
   * @method getVisible
   * @memberOf lil.Object.prototype
   * @return {Boolean} True if visible
   */

  /**
   * Sets object's {@link lil.Object#visible|visible} state
   * @method setVisible
   * @memberOf lil.Object.prototype
   * @param {Boolean} value visible value
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#shadow|shadow}
   * @method getShadow
   * @memberOf lil.Object.prototype
   * @return {Object} Shadow instance
   */

  /**
   * Retrieves object's {@link lil.Object#stroke|stroke}
   * @method getStroke
   * @memberOf lil.Object.prototype
   * @return {String} stroke value
   */

  /**
   * Sets object's {@link lil.Object#stroke|stroke}
   * @method setStroke
   * @memberOf lil.Object.prototype
   * @param {String} value stroke value
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#strokeWidth|strokeWidth}
   * @method getStrokeWidth
   * @memberOf lil.Object.prototype
   * @return {Number} strokeWidth value
   */

  /**
   * Sets object's {@link lil.Object#strokeWidth|strokeWidth}
   * @method setStrokeWidth
   * @memberOf lil.Object.prototype
   * @param {Number} value strokeWidth value
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#originX|originX}
   * @method getOriginX
   * @memberOf lil.Object.prototype
   * @return {String} originX value
   */

  /**
   * Sets object's {@link lil.Object#originX|originX}
   * @method setOriginX
   * @memberOf lil.Object.prototype
   * @param {String} value originX value
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#originY|originY}
   * @method getOriginY
   * @memberOf lil.Object.prototype
   * @return {String} originY value
   */

  /**
   * Sets object's {@link lil.Object#originY|originY}
   * @method setOriginY
   * @memberOf lil.Object.prototype
   * @param {String} value originY value
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#fill|fill}
   * @method getFill
   * @memberOf lil.Object.prototype
   * @return {String} Fill value
   */

  /**
   * Sets object's {@link lil.Object#fill|fill}
   * @method setFill
   * @memberOf lil.Object.prototype
   * @param {String} value Fill value
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#opacity|opacity}
   * @method getOpacity
   * @memberOf lil.Object.prototype
   * @return {Number} Opacity value (0-1)
   */

  /**
   * Sets object's {@link lil.Object#opacity|opacity}
   * @method setOpacity
   * @memberOf lil.Object.prototype
   * @param {Number} value Opacity value (0-1)
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#angle|angle} (in degrees)
   * @method getAngle
   * @memberOf lil.Object.prototype
   * @return {Number}
   */

  /**
   * Retrieves object's {@link lil.Object#top|top position}
   * @method getTop
   * @memberOf lil.Object.prototype
   * @return {Number} Top value (in pixels)
   */

  /**
   * Sets object's {@link lil.Object#top|top position}
   * @method setTop
   * @memberOf lil.Object.prototype
   * @param {Number} value Top value (in pixels)
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#left|left position}
   * @method getLeft
   * @memberOf lil.Object.prototype
   * @return {Number} Left value (in pixels)
   */

  /**
   * Sets object's {@link lil.Object#left|left position}
   * @method setLeft
   * @memberOf lil.Object.prototype
   * @param {Number} value Left value (in pixels)
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#scaleX|scaleX} value
   * @method getScaleX
   * @memberOf lil.Object.prototype
   * @return {Number} scaleX value
   */

  /**
   * Sets object's {@link lil.Object#scaleX|scaleX} value
   * @method setScaleX
   * @memberOf lil.Object.prototype
   * @param {Number} value scaleX value
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#scaleY|scaleY} value
   * @method getScaleY
   * @memberOf lil.Object.prototype
   * @return {Number} scaleY value
   */

  /**
   * Sets object's {@link lil.Object#scaleY|scaleY} value
   * @method setScaleY
   * @memberOf lil.Object.prototype
   * @param {Number} value scaleY value
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#flipX|flipX} value
   * @method getFlipX
   * @memberOf lil.Object.prototype
   * @return {Boolean} flipX value
   */

  /**
   * Sets object's {@link lil.Object#flipX|flipX} value
   * @method setFlipX
   * @memberOf lil.Object.prototype
   * @param {Boolean} value flipX value
   * @return {lil.Object} thisArg
   * @chainable
   */

  /**
   * Retrieves object's {@link lil.Object#flipY|flipY} value
   * @method getFlipY
   * @memberOf lil.Object.prototype
   * @return {Boolean} flipY value
   */

  /**
   * Sets object's {@link lil.Object#flipY|flipY} value
   * @method setFlipY
   * @memberOf lil.Object.prototype
   * @param {Boolean} value flipY value
   * @return {lil.Object} thisArg
   * @chainable
   */

})(typeof exports !== 'undefined' ? exports : this);
