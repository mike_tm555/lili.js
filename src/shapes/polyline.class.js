(function(global) {

  'use strict';

  var lil = global.lil || (global.lil = { });
  var min = lil.util.array.min;
  var max = lil.util.array.max;
  var extend = lil.util.object.extend;
  var toFixed = lil.util.toFixed;

  if (lil.Polyline) {
    lil.warn('lil.Polyline is already defined');
    return;
  }

  /**
   * Polyline class
   * @class lil.Polyline
   * @extends lil.Object
   * @see {@link lil.Polyline#initialize} for constructor definition
   */
  lil.Polyline = lil.util.createClass(lil.Object, /** @lends lil.Polyline.prototype */ {

    /**
     * Type of an object
     * @type String
     * @default
     */
    type: 'polyline',

    /**
     * Points array
     * @type Array
     * @default
     */
    points: null,

    /**
     * Minimum X from points values, necessary to offset points
     * @type Number
     * @default
     */
    minX: 0,

    /**
     * Minimum Y from points values, necessary to offset points
     * @type Number
     * @default
     */
    minY: 0,

    /**
     * Constructor
     * @param {Array} points Array of points (where each point is an object with x and y)
     * @param {Object} [options] Options object
     * @return {lil.Polyline} thisArg
     * @example
     * var poly = new lil.Polyline([
     *     { x: 10, y: 10 },
     *     { x: 50, y: 30 },
     *     { x: 40, y: 70 },
     *     { x: 60, y: 50 },
     *     { x: 100, y: 150 },
     *     { x: 40, y: 100 }
     *   ], {
     *   stroke: 'red',
     *   left: 100,
     *   top: 100
     * });
     */
    initialize: function(points, options) {
      options = options || {};
      this.points = points || [];
      this.callSuper('initialize', options);
      this._calcDimensions();

      if (!('top' in options)) {
        this.top = this.minY;
      }
      if (!('left' in options)) {
        this.left = this.minX;
      }

      this.pathOffset = {
        x: this.minX + this.width / 2,
        y: this.minY + this.height / 2
      };
    },

    /**
     * @private
     */
    _calcDimensions: function () {
      var points = this.points;
      var minX = min(points, 'x');
      var minY = min(points, 'y');
      var maxX = max(points, 'x');
      var maxY = max(points, 'y');
      this.width = (maxX - minX) || 0;
      this.height = (maxY - minY) || 0;
      this.minX = minX || 0;
      this.minY = minY || 0;
    },

    _setPositionDimensions: function(options) {
      var calcDim = this._calcDimensions(options), correctLeftTop;
      this.width = calcDim.width;
      this.height = calcDim.height;
      if (!options.fromSVG) {
        correctLeftTop = this.translateToGivenOrigin(
          { x: calcDim.left - this.strokeWidth / 2, y: calcDim.top - this.strokeWidth / 2 },
          'left',
          'top',
          this.originX,
          this.originY
        );
      }
      if (typeof options.left === 'undefined') {
        this.left = options.fromSVG ? calcDim.left : correctLeftTop.x;
      }
      if (typeof options.top === 'undefined') {
        this.top = options.fromSVG ? calcDim.top : correctLeftTop.y;
      }
      this.pathOffset = {
        x: calcDim.left + this.width / 2,
        y: calcDim.top + this.height / 2
      };
    },

    /**
     * Returns object representation of an instance
     * @param {Array} [propertiesToInclude] Any properties that you might want to additionally include in the output
     * @return {Object} Object representation of an instance
     */
    toObject: function (propertiesToInclude) {
      return extend(this.callSuper('toObject', propertiesToInclude), {
        points: this.points.concat()
      });
    },

    /* _TO_SVG_START_ */
    /**
     * Returns svg representation of an instance
     * @param {Function} [reviver] Method for further parsing of svg representation.
     * @return {String} svg representation of an instance
     */
    toSVG: function (reviver) {
      var points = [];
      var markup = this._createBaseSVGMarkup();

      for (var i = 0, len = this.points.length; i < len; i++) {
        points.push(toFixed(this.points[i].x, 2), ',', toFixed(this.points[i].y, 2), ' ');
      }

      markup.push(
        '<', this.type, ' ', this.getSvgId(),
        'points="', points.join(''),
        '" style="', this.getSvgStyles(),
        '"/>\n'
      );

      return reviver ? reviver(markup.join('')) : markup.join('');
    },
    /* _TO_SVG_END_ */

    /**
     * @private
     * @param {CanvasRenderingContext2D} ctx Context to render on
     */
    _render: function(ctx, noTransform) {
      if (!lil.Polygon.prototype.commonRender.call(this, ctx, noTransform)) {
        return;
      }
      this._renderFill(ctx);
      this._renderStroke(ctx);
    },

    /**
     * @private
     * @param {CanvasRenderingContext2D} ctx Context to render on
     */
    _renderDashedStroke: function(ctx) {
      var p1, p2;

      ctx.beginPath();
      for (var i = 0, len = this.points.length; i < len; i++) {
        p1 = this.points[i];
        p2 = this.points[i + 1] || p1;
        lil.util.drawDashedLine(ctx, p1.x, p1.y, p2.x, p2.y, this.strokeDashArray);
      }
    },

    /**
     * Returns complexity of an instance
     * @return {Number} complexity of this instance
     */
    complexity: function() {
      return this.get('points').length;
    }
  });

  /* _FROM_SVG_START_ */
  /**
   * List of attribute names to account for when parsing SVG element (used by {@link lil.Polyline.fromElement})
   * @static
   * @memberOf lil.Polyline
   * @see: http://www.w3.org/TR/SVG/shapes.html#PolylineElement
   */
  lil.Polyline.ATTRIBUTE_NAMES = lil.SHARED_ATTRIBUTES.concat();

  /**
   * Returns lil.Polyline instance from an SVG element
   * @static
   * @memberOf lil.Polyline
   * @param {SVGElement} element Element to parser
   * @param {Function} callback callback function invoked after parsing
   * @param {Object} [options] Options object
   */
  lil.Polyline.fromElementGenerator = function(_class) {
    return function(element, callback, options) {
      if (!element) {
        return callback(null);
      }
      options || (options = { });

      var points = lil.parsePointsAttribute(element.getAttribute('points'));
      var parsedAttributes = lil.parseAttributes(element, lil[_class].ATTRIBUTE_NAMES);
      parsedAttributes.fromSVG = true;
      callback(new lil[_class](points, lil.util.object.extend(parsedAttributes, options)));
    };
  };

  lil.Polyline.fromElement = lil.Polyline.fromElementGenerator('Polyline');

  /**
   * Returns lil.Polyline instance from an object representation
   * @static
   * @memberOf lil.Polyline
   * @param {Object} object Object to create an instance from
   * @return {lil.Polyline} Instance of lil.Polyline
   */
  lil.Polyline.fromObject = function(object) {
    var points = object.points;
    return new lil.Polyline(points, object, true);
  };

})(typeof exports !== 'undefined' ? exports : this);
