(function (global) {

  'use strict';

  var lil = global.lil || (global.lil = {});

  if (lil.Polygon) {
    lil.warn('lil.Polygon is already defined');
    return;
  }

  /**
   * Polygon class
   * @class lil.Polygon
   * @extends lil.Polyline
   * @see {@link lil.Polygon#initialize} for constructor definition
   */
  lil.Polygon = lil.util.createClass(lil.Polyline, /** @lends lil.Polygon.prototype */ {
    /**
     * Type of an object
     * @type String
     * @default
     */
    type: 'polygon',

    /**
     * Points array
     * @type Array
     * @default
     */
    points: null,

    /**
     * Minimum X from points values, necessary to offset points
     * @type Number
     * @default
     */
    minX: 0,

    /**
     * Minimum Y from points values, necessary to offset points
     * @type Number
     * @default
     */
    minY: 0,

    /**
     * @private
     * @param {CanvasRenderingContext2D} ctx Context to render on
     */
    _render: function (ctx, noTransform) {
      if (!this.commonRender(ctx, noTransform)) {
        return;
      }
      this._renderFill(ctx);
      if (this.stroke || this.strokeDashArray) {
        ctx.closePath();
        this._renderStroke(ctx);
      }
    },

    /**
     * @private
     * @param {CanvasRenderingContext2D} ctx Context to render on
     */
    commonRender: function (ctx, noTransform) {
      var point, len = this.points.length;

      if (!len || isNaN(this.points[len - 1].y)) {
        // do not draw if no points or odd points
        // NaN comes from parseFloat of a empty string in parser
        return false;
      }

      noTransform || ctx.translate(-this.pathOffset.x, -this.pathOffset.y);
      ctx.beginPath();
      ctx.moveTo(this.points[0].x, this.points[0].y);
      for (var i = 0; i < len; i++) {
        point = this.points[i];
        ctx.lineTo(point.x, point.y);
      }
      return true;
    },

    /**
     * @private
     * @param {CanvasRenderingContext2D} ctx Context to render on
     */
    _renderDashedStroke: function (ctx) {
      lil.Polyline.prototype._renderDashedStroke.call(this, ctx);
      ctx.closePath();
    },

    /**
     * Returns complexity of an instance
     * @return {Number} complexity of this instance
     */
    complexity: function () {
      return this.points.length;
    }
  });

  /* _FROM_SVG_START_ */
  /**
   * List of attribute names to account for when parsing SVG element (used by `lil.Polygon.fromElement`)
   * @static
   * @memberOf lil.Polygon
   * @see: http://www.w3.org/TR/SVG/shapes.html#PolygonElement
   */
  lil.Polygon.ATTRIBUTE_NAMES = lil.SHARED_ATTRIBUTES.concat();

  /**
   * Returns {@link lil.Polygon} instance from an SVG element
   * @static
   * @memberOf lil.Polygon
   * @param {SVGElement} element Element to parse
   * @param {Function} callback callback function invoked after parsing
   * @param {Object} [options] Options object
   */
  lil.Polygon.fromElement = lil.Polyline.fromElementGenerator('Polygon');
  /* _FROM_SVG_END_ */

  /**
   * Returns lil.Polygon instance from an object representation
   * @static
   * @memberOf lil.Polygon
   * @param {Object} object Object to create an instance from
   * @param {Function} [callback] Callback to invoke when an lil.Path instance is created
   */
  lil.Polygon.fromObject = function (object, callback) {
    return lil.Object._fromObject('Polygon', object, callback, 'points');
  };
})(typeof exports !== 'undefined' ? exports : this);
