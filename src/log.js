/**
 * Wrapper around `console.log` (when available)
 * @param {*} [values] Values to log
 */
lil.log = console.log;

/**
 * Wrapper around `console.warn` (when available)
 * @param {*} [values] Values to log as a warning
 */
lil.warn = console.warn;
