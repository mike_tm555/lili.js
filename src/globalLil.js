if (typeof document !== 'undefined' && typeof window !== 'undefined') {
  // ensure globality even if entire library were function wrapped (as in Meteor.js packaging system)
  window.lil = lil;
  window.onload = function () {
    /** @ignore */
    lil.StaticCanvas.prototype.createPNGStream = function() {
      return this.nodeCanvas.createPNGStream();
    };

    lil.StaticCanvas.prototype.createJPEGStream = function(opts) {
      return this.nodeCanvas.createJPEGStream(opts);
    };

    var origSetWidth = lil.StaticCanvas.prototype.setWidth;
    lil.StaticCanvas.prototype.setWidth = function(width, options) {
      origSetWidth.call(this, width, options);
      this.nodeCanvas.width = width;
      return this;
    };
    if (lil.Canvas) {
      lil.Canvas.prototype.setWidth = lil.StaticCanvas.prototype.setWidth;
    }

    var origSetHeight = lil.StaticCanvas.prototype.setHeight;
    lil.StaticCanvas.prototype.setHeight = function(height, options) {
      origSetHeight.call(this, height, options);
      this.nodeCanvas.height = height;
      return this;
    };

    lil.Point.prototype.angleRelativeFrom = function (startpoint) {
      return Math.atan2(this.y - startpoint.y, this.x - startpoint.x) * 180 / Math.PI;
    };

    if (lil.Canvas) {
      lil.Canvas.prototype.setHeight = lil.StaticCanvas.prototype.setHeight;
    }

    lil.StaticCanvas.prototype.getObjectById = function (id) {
      var obj = undefined;
      this.getObjects().some(function (o) {
        if (o.hasOwnProperty('id')) {
          if (o.id === id) {
            obj = o;
            return true;
          }
        }
        return false;
      });
      return obj;
    };
  };
}
